# Vault-101 Overview

Here are the lab notes for the [HashiCorp Vault-101 - Certified Vault Associate Course.](https://courses.tekanaid.com/p/hashicorp-vault-101-certified-vault-associate)

## Structure

This repo is broken down by the course sections. Some sections don't have labs. Below is a list of labs and in what sections they exist:

### Labs
Lab01 [Section03] - Vault Installation
Lab02 [Section04] - The Vault CLI
Lab03 [Section04] - Vault Server in Dev Mode
Lab04 [Section04] - The Vault API
Lab05 [Section06] - Run a Vault Production Server
Lab06 [Section06] - Auto Unseal with AWS KMS
Lab07 [Section07] - Userpass Auth Method
Lab08 [Section07] - AppRole Auth Method
Lab09 [Section07] - Vault Entities, Aliases, and Identity Groups
Lab10 [Section08] - Policies Lab 1
Lab11 [Section08] - Policies Lab 2
Lab12 [Section09] - Tokens Lab 1
Lab13 [Section09] - Tokens Lab 2
Lab14a [Section10] - KV Secrets Engine Lab 1
Lab14b [Section10] - KV Secrets Engine Lab 2
Lab15 [Section10] - AWS Secrets Engine Lab
Lab16 [Section10] - Transit Secrets Engine Lab
Lab17 [Section10] - Cubbyhole Secrets Engine Lab
Lab18 [Section10] - Cubbyhole Response Wrapping Lab
Lab19 [Section11] - Replication Design Lab
Lab20 [Section12] - The Vault Agent Lab
